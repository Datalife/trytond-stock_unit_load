#!/usr/bin/env python
import argparse


def parse_commandline():
    options = {}

    parser = argparse.ArgumentParser(prog='update_bom_amount')

    parser.add_argument("-c", "--config", dest="configfile", metavar='FILE',
        default=None, help="specify config file")
    parser.add_argument("-d", "--database", dest="database_name", default=None,
        metavar='DATABASE', help="specify the database name")
    parser.add_argument("-cp", "--company", dest="company_id", default=None,
        metavar='COMPANY', help="specify the company ID")

    options = parser.parse_args()

    if not options.database_name:
        parser.error('Missing database option')
    if not options.company_id:
        parser.error('Missing company option')
    return options


if __name__ == '__main__':

    options = parse_commandline()

    from trytond.transaction import Transaction
    from trytond.pool import Pool
    from trytond.config import config as CONFIG
    from trytond.tools import grouped_slice, reduce_ids

    CONFIG.update_etc(options.configfile)

    Pool.start()
    pool = Pool(options.database_name)
    pool.init()
    context = {'company': options.company_id}

    with Transaction().start(options.database_name, 1, context=context):
        UnitLoad = pool.get('stock.unit_load')
        unit_load = UnitLoad.__table__()
        cursor = Transaction().connection.cursor()

        def set_warehouse(extra_domain=[], key=''):
            domain = [('at_warehouse', '=', None)]
            domain.extend(extra_domain)

            records = UnitLoad.search(domain)

            print('Total records: %s' % len(records))
            values = {}
            for grouped_records in grouped_slice(records):
                print('> Iterate grouped records')
                for record in grouped_records:
                    if key == 'shipment_out':
                        wh = record.shipment.warehouse
                    elif key == 'shipment_internal':
                        wh = record.shipment.to_location.warehouse
                    elif key == 'receipt':
                        wh = record.goods_receipt_line.receipt.warehouse
                    elif key == 'warehouse':
                        wh = record.warehouse
                    elif not key:
                        wh = record.get_at_warehouse()
                    else:
                        raise NotImplementedError()
                    if wh:
                        values.setdefault(wh, []).append(record.id)

            print('Start to update ULs')
            for warehouse, uls in values.items():
                print('Warehouse "%s"' % warehouse.rec_name)
                cursor.execute(*unit_load.update(
                    columns=[unit_load.at_warehouse],
                    values=[warehouse.id],
                    where=reduce_ids(unit_load.id, uls)))
                Transaction().commit()

        # shipped to customer
        set_warehouse(
            extra_domain=[('shipment', 'like', 'stock.shipment.out,%')],
            key='shipment_out')
        # returned from customer
        set_warehouse(
            extra_domain=[
                ('shipment', 'like', 'stock.shipment.out.return,%'),
                ('shipment.state', '=', 'done', 'stock.shipment.out.return')],
            key='shipment_out')
        # moved with internal shipment
        set_warehouse(
            extra_domain=[
                ('shipment', 'like', 'stock.shipment.internal,%'),
                ('shipment.state', '=', 'done', 'stock.shipment.internal')],
            key='shipment_internal')
        # agro goods receipt
        set_warehouse(
            extra_domain=[
                ('shipment', '=', None),
                ('goods_receipt_line', '!=', None),
                ('goods_receipt_line.receipt.state', '=', 'done')
            ],
            key='receipt')
        # warehouse production
        set_warehouse(
            extra_domain=[
                ('shipment', '=', None),
                ('warehouse', '!=', None),
            ],
            key='warehouse')
        # others
        set_warehouse()
