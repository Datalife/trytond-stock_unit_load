import fileinput
from xmlrpc import client

HOST = '<HOST>'
PORT = '<PORT>'
DB = '<DB>'
USER = '<USER>'
PASSWORD = '<PWD>'

PRODUCTION_LOCATION_PREFIX = ['M']


class Tryton(object):

    def __init__(self):
        # Get user_id and session
        self.server = client.ServerProxy('http://%s:%s@%s:%s/%s/' % (
            USER, PASSWORD, HOST, PORT, DB), allow_none=True)

        # Get the user context
        self.pref = self.server.model.res.user.get_preferences(True, {})

    def execute(self, method, *args):
        args += (self.pref,)
        return getattr(self.server, method)(*args)

    def drop_ul(self, ul, location):
        self.execute(
            'model.stock.unit_load.auto_drop', ul, location)


def scanner():
    server = Tryton()
    location = None
    print('Awaiting for input...')
    for text in fileinput.input():
        if text[0] in PRODUCTION_LOCATION_PREFIX:
            location = text.replace('\n', '')
            print('Drop location changed: ', location)
        else:
            if location:
                ul = text.replace('\n', '')
                try:
                    server.drop_ul(ul, location)
                    print('Dropped unit load nº', ul)
                except Exception as e:
                    print('Error encountered: ', e)
                    server = Tryton()
            else:
                print('No location defined!')


if __name__ == "__main__":
    scanner()
