# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Id
from trytond.transaction import Transaction
from trytond.tools.multivalue import migrate_property
from trytond.modules.company.model import CompanyValueMixin
from trytond import backend
from sql import Table


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    unit_load_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence', 'Load unit Sequence', required=True,
            domain=[
                ('sequence_type', '=', Id('stock_unit_load',
                    'sequence_type_unit_load')),
                ('company', 'in', [Eval('context', {}
                    ).get('company', -1), None])
            ]))
    ul_production_type = fields.MultiValue(
        fields.Selection([('location', 'Location')],
            'UL Production type', required=True))
    pallet_category = fields.Many2One('product.category', 'Pallet Category')
    case_category = fields.Many2One('product.category', 'Case Category')
    do_ul_drop = fields.MultiValue(fields.Boolean('Do Unit Load move on drop',
        help='If checked drop UL moves will be done.'))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'unit_load_sequence':
            return pool.get('stock.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_unit_load_sequence(cls, **pattern):
        return cls.multivalue_model(
            'unit_load_sequence').default_unit_load_sequence()

    @classmethod
    def default_ul_production_type(cls, **pattern):
        return cls.multivalue_model(
            'ul_production_type').default_ul_production_type()

    @classmethod
    def default_do_ul_drop(cls, **pattern):
        return cls.multivalue_model(
            'do_ul_drop').default_do_ul_drop()

    @classmethod
    def write(cls, *args):
        UnitLoad = Pool().get('stock.unit_load')
        super().write(*args)
        UnitLoad._product_category_cache.clear()


class ConfigurationSequence(metaclass=PoolMeta):
    __name__ = 'stock.configuration.sequence'

    unit_load_sequence = fields.Many2One('ir.sequence', 'Load unit Sequence',
        required=True,
        domain=[
            ('sequence_type', '=', Id('stock_unit_load',
                'sequence_type_unit_load')),
            ('company', 'in', [Eval('context', {}).get(
                    'company', -1), None])
        ])

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)
        if exist:
            exist &= table_h.column_exist('unit_load_sequence')

        super(ConfigurationSequence, cls).__register__(module_name)

        if not exist:
            # Re-migration
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('unit_load_sequence')
        value_names.append('unit_load_sequence')
        super(ConfigurationSequence, cls)._migrate_property(field_names,
            value_names, fields)

    @classmethod
    def default_unit_load_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('stock_unit_load', 'sequence_unit_load')
        except KeyError:
            return None


class ConfigurationDoUlDrop(ModelSQL, CompanyValueMixin):
    "Configuration Do Ul Drop"

    __name__ = 'stock.configuration.do_ul_drop'

    do_ul_drop = fields.Boolean('Do Unit Load move on drop')

    @classmethod
    def default_do_ul_drop(cls):
        return False


class ConfigurationULProductionType(ModelSQL, CompanyValueMixin):
    "Stock Configuration UL Production Type"
    __name__ = 'stock.configuration.ul_production_type'

    ul_production_type = fields.Selection([('location', 'Location')],
            'UL Production type', required=True)

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)

        super(ConfigurationULProductionType, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('ul_production_type')
        value_names.append('ul_production_type')
        fields.append('company')
        migrate_property('stock.configuration',
            field_names, cls, value_names, fields=fields)

    @classmethod
    def default_ul_production_type(cls):
        return 'location'


class ProductionConfiguration(metaclass=PoolMeta):
    __name__ = 'production.configuration'

    propose_drop_end_date = fields.MultiValue(
        fields.Selection([
            (None, ''),
            ('start_date', 'Start date'),
            ('last_drop', 'Last drop'),
        ], 'Propose drop UL end date',
            help="Determines how drop UL move''s end date will be proposed."))

    @classmethod
    def default_propose_drop_end_date(cls, **pattern):
        return cls.multivalue_model(
            'propose_drop_end_date').default_propose_drop_end_date()


class ConfigurationProposeDropEndDate(ModelSQL, CompanyValueMixin):
    "Configuration Propose Drop End Date"

    __name__ = 'production.configuration.propose_drop_end_date'

    propose_drop_end_date = fields.Selection([
        (None, ''),
        ('start_date', 'Start date'),
        ('last_drop', 'Last drop')], 'Propose drop UL end date')

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.TableHandler
        table_h = cls.__table_handler__(module_name)
        cursor = Transaction().connection.cursor()

        old_table = 'stock_configuration_propose_drop_end_date'
        to_update, values = False, []
        if TableHandler.table_exist(old_table):
            TableHandler.table_rename(old_table, cls._table)

            to_update = True
            sql_table = Table(old_table)
            cursor.execute(*sql_table.select(
                sql_table.id,
                where=sql_table.propose_drop_end_date == True))
            values = [r[0] for r in cursor.fetchall()]
            table_h.drop_column('propose_drop_end_date')

        super().__register__(module_name)

        if to_update and values:
            sql_table = cls.__table__()
            cursor.execute(*sql_table.update(
                columns=[sql_table.propose_drop_end_date],
                values=['start_date'],
                where=sql_table.id.in_(values)))

    @classmethod
    def default_propose_drop_end_date(cls):
        return None
