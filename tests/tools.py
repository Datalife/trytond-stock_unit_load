# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

import datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from proteus import Model
today = datetime.date.today()

__all__ = ['create_unit_load']


def _create_product():
    """Create product"""
    ProductUom = Model.get('product.uom')
    ProductTemplate = Model.get('product.template')
    Product = Model.get('product.product')
    unit, = ProductUom.find([('name', '=', 'Unit')])
    product = Product()
    template = ProductTemplate()
    template.name = 'Product'
    template.default_uom = unit
    template.type = 'goods'
    template.list_price = Decimal('20')
    template.save()
    product, = template.products
    product.cost_price = Decimal('8')
    product.template = template
    product.save()
    return product


def create_unit_load(config=None, product=None, do_state=True,
                     default_values={}):
    """Create unit load"""
    Location = Model.get('stock.location')
    UnitLoad = Model.get('stock.unit_load')

    if not product:
        product = _create_product()

    wh, = Location.find([('code', '=', 'WH')])
    production_loc = Location.find([('type', '=', 'production')], limit=1)
    if production_loc:
        production_loc, = production_loc
    else:
        production_loc = Location(name='Production', code='PL')
        production_loc.type = 'production'
        production_loc.save()

    unit_load = UnitLoad(**default_values)
    unit_load.end_date = unit_load.start_date + relativedelta(minutes=5)
    unit_load.production_type = 'location'
    unit_load.warehouse = wh
    unit_load.production_location = production_loc
    unit_load.product = product
    unit_load.cases_quantity = 5
    unit_load.quantity = Decimal('35.0')
    unit_load.save()
    if do_state:
        unit_load.click('do')
    return unit_load
