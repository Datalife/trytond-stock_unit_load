========================
Check unit load creation
========================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard, Report
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.stock_unit_load.tests.tools import create_unit_load
    >>> today = datetime.date.today()
    >>> tomorrow = today + relativedelta(days=1)
    >>> time_ = datetime.datetime.now().time()
    >>> time_ = time_.replace(microsecond=0)

Install unit load Module::

    >>> config = activate_modules('stock_unit_load')

Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('8')
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> wh2, = warehouse_loc.duplicate()
    >>> wh2.name = 'Warehouse 2'
    >>> wh2.code = 'WH2'
    >>> wh2.save()

Create an unit load::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.company != None
    True
    >>> unit_load.start_date != None
    True
    >>> unit_load.end_date != None
    True
    >>> unit_load.end_date = unit_load.start_date
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = output_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> len(unit_load.production_moves)
    0
    >>> unit_load.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Production location" in "Unit load" is not valid according to its domain. - 

    >>> unit_load.production_location = production_loc
    >>> unit_load.save()
    >>> unit_load.code != None
    True
    >>> unit_load.shipment == None
    True
    >>> unit_load.at_warehouse == None
    True

Add moves::

    >>> unit_load.production_state
    'running'
    >>> unit_load.available
    False
    >>> unit_load.quantity = Decimal('35.0')
    >>> len(unit_load.production_moves)
    1
    >>> move = unit_load.production_moves[0]
    >>> move.planned_date == today
    True
    >>> move.product == unit_load.product
    True
    >>> move.quantity
    35.0
    >>> move.from_location == production_loc
    True
    >>> not move.to_location
    True
    >>> move.to_location = storage_loc
    >>> move.currency == unit_load.company.currency
    True
    >>> unit_load.save()
    >>> unit_load.state
    'draft'
    >>> unit_load.production_state
    'running'
    >>> len(unit_load.moves)
    1
    >>> len(unit_load.production_moves)
    1
    >>> unit_load.production_moves[0].state
    'draft'
    >>> unit_load.internal_quantity
    35.0
    >>> unit_load.quantity_per_case
    7.0

Check computed fields::

    >>> unit_load.location.code
    'STO'
    >>> len(unit_load.last_moves) == 1
    True
    >>> unit_load.click('assign')
    >>> not unit_load.at_warehouse
    True
    >>> unit_load.state
    'assigned'
    >>> unit_load.moves[0].state
    'assigned'
    >>> unit_load.production_state
    'running'
    >>> len(unit_load.ul_moves)
    1
    >>> unit_load.uom.rec_name
    'Unit'
    >>> unit_load.save()

Move wizard::

    >>> move_ul = Wizard('stock.unit_load.do_move', [unit_load])
    >>> move_ul.form.planned_date = datetime.datetime.combine(today - relativedelta(days=1), time_)
    >>> move_ul.form.location = storage_loc
    >>> move_ul.form.warehouse == storage_loc.warehouse
    True
    >>> move_ul.form.planned_date = datetime.datetime.combine(today - relativedelta(days=20), time_)

State error::

    >>> move_ul.execute('move_') # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Unit load "..." must be in Done state before moving. - 
    >>> unit_load.click('do')
    >>> unit_load.state
    'done'
    >>> unit_load.production_state
    'done'
    >>> unit_load.quantity
    35.0
    >>> unit_load.forecast_quantity
    35.0
    >>> unit_load.at_warehouse == storage_loc.warehouse
    True

Location error::

    >>> move_ul.execute('move_') # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot move unit load "..." to Location "Storage Zone". Check its movements. - 

    >>> move_ul.form.location = output_loc


Done error::

    >>> unit_load.quantity = 40.0
    >>> unit_load.save() # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Unit load "..." is done. - 


Date error::

    >>> move_ul.execute('move_') # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot move unit load "..." at date "..." because later moves exist. - 

    >>> move_ul.form.planned_date = datetime.datetime.combine(tomorrow, time_)
    >>> move_ul.execute('move_')
    >>> unit_load.reload()
    >>> unit_load.state
    'draft'
    >>> len(unit_load.moves)
    2
    >>> unit_load.last_date == datetime.datetime.combine(tomorrow, time_)
    True


Cancel last move::

    >>> unit_load.location.id == output_loc.id
    True
    >>> unit_load.click('cancel')
    >>> unit_load.reload()
    >>> unit_load.location.id == storage_loc.id
    True
    >>> unit_load.state
    'done'

Create customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create a shipment out::

    >>> ShipmentOut = Model.get('stock.shipment.out')
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.start_date = datetime.datetime.now() + relativedelta(minutes=10)
    >>> shipment_out.end_date = shipment_out.start_date + relativedelta(minutes=30)
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company

Add Unit load::

    >>> unit_load.available
    True
    >>> unit_load.at_warehouse == storage_loc.warehouse
    True
    >>> shipment_out.unit_loads.append(unit_load)
    >>> len(shipment_out.outgoing_moves)
    1
    >>> shipment_out.outgoing_moves[0].unit_load.id == unit_load.id
    True
    >>> shipment_out.save()
    >>> len(shipment_out.outgoing_moves)
    1
    >>> shipment_out.click('wait')
    >>> len(shipment_out.inventory_moves)
    1
    >>> shipment_out.inventory_moves[0].unit_load.id == unit_load.id
    True
    >>> unit_load.reload()
    >>> unit_load.at_warehouse == storage_loc.warehouse
    True
    >>> shipment_out.click('assign_try')
    True
    >>> shipment_out.click('pick')
    >>> shipment_out.click('pack')
    >>> shipment_out.click('done')
    >>> unit_load.reload()
    >>> unit_load.at_warehouse == storage_loc.warehouse
    True

Check unit load state::

    >>> unit_load.reload()
    >>> len(unit_load.ul_moves)
    3
    >>> unit_load.state
    'done'
    >>> unit_load.location.id == customer.customer_location.id
    True
    >>> unit_load.shipment == shipment_out
    True
    >>> unit_load.available
    False

Create a shipment out return::

    >>> time_ = time_.replace(hour=time_.hour + 1)
    >>> unit_load = UnitLoad(unit_load.id)
    >>> ShipmentOutReturn = Model.get('stock.shipment.out.return')
    >>> shipment_out_return = ShipmentOutReturn()
    >>> shipment_out_return.customer = customer
    >>> shipment_out_return.warehouse = warehouse_loc
    >>> shipment_out_return.company = company
    >>> shipment_out_return.planned_date = today
    >>> shipment_out_return.effective_date = today
    >>> shipment_out_return.start_time = time_
    >>> shipment_out_return.unit_loads.append(unit_load)
    >>> shipment_out_return.save()
    >>> len(shipment_out_return.incoming_moves)
    1
    >>> shipment_out_return.incoming_moves[0].unit_load != None
    True
    >>> shipment_out_return.click('receive')
    >>> unit_load.reload()
    >>> unit_load.shipment == shipment_out_return
    True
    >>> unit_load.available
    True
    >>> len(shipment_out_return.inventory_moves)
    1
    >>> shipment_out_return.inventory_moves[0].unit_load == unit_load
    True

Check report::

    >>> label = Report('stock.unit_load.label')
    >>> ext, _, _, name = label.execute([unit_load], {})
    >>> ext
    'odt'
    >>> name == ('Label-%s' % unit_load.rec_name)
    True

Check report case_label::

    >>> case_label = Report('stock.unit_load.case_label')
    >>> ext, _, _, name = case_label.execute([unit_load], {})
    >>> ext
    'odt'
    >>> name == ('Case Label-%s' % unit_load.rec_name)
    True

Create another unit load and try move to another warehouse::

    >>> unit_load2 = create_unit_load(default_values={'start_date': datetime.datetime.combine(today - relativedelta(days=2), time_)})
    >>> move_ul = Wizard('stock.unit_load.do_move', [unit_load2])
    >>> move_ul.form.planned_date = datetime.datetime.combine(today - relativedelta(days=1), time_)
    >>> move_ul.form.location = wh2.storage_location
    >>> move_ul.execute('move_')
    >>> unit_load2.click('do') # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot move or drop UL "..." to location "Storage Zone" because it is on Warehouse "Warehouse". - 

Create an unit load in a warehouse and change it later::

    >>> unit_load = UnitLoad()
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=5)
    >>> unit_load.warehouse = warehouse_loc
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = warehouse_loc.production_location
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.save()
    >>> unit_load.at_warehouse == warehouse_loc
    True
    >>> unit_load.warehouse = wh2
    >>> unit_load.save()
    >>> unit_load.at_warehouse == wh2
    True

Check error in last moves::

    >>> unit_load = UnitLoad()
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=5)
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = output_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.production_location = production_loc
    >>> unit_load.quantity = Decimal('35.0')
    >>> move = unit_load.production_moves[0]
    >>> move.to_location = storage_loc
    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: The moves of the unit load cannot be done because have date in future. - 