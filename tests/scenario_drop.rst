===============
Drop unit load
===============

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard, Report
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.stock_unit_load.tests.tools import create_unit_load
    >>> today = datetime.date.today()
    >>> tomorrow = today + relativedelta(days=1)
    >>> time_ = datetime.datetime.now().time()
    >>> time_ = time_.replace(microsecond=0)
    >>> import time

Install unit load Module::

    >>> config = activate_modules(['stock_unit_load'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create main product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('8')
    >>> product.save()

Create another product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> template = ProductTemplate()
    >>> template.name = 'Aux. product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('0')
    >>> template.save()
    >>> aux_product, = template.products
    >>> aux_product.cost_price = Decimal('2')
    >>> aux_product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> new_stor = Location(name='Storage 2')
    >>> new_stor.parent = storage_loc
    >>> new_stor.save()
    >>> storage3 = Location(name='Storage 3')
    >>> storage3.parent = storage_loc
    >>> storage3.save()
    >>> production_loc2 = Location(name='Production 2', code='PROD2',
    ...     type='production', parent=production_loc)
    >>> production_loc2.save()

Create an unit load::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=15)
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.save()

Add moves::

    >>> unit_load.quantity = Decimal('35.0')
    >>> move = unit_load.production_moves[0]
    >>> move.to_location = new_stor
    >>> input_move = unit_load.production_moves.new()
    >>> input_move.product = aux_product
    >>> input_move.quantity = Decimal('4')
    >>> input_move.from_location = storage_loc
    >>> input_move.to_location = production_loc
    >>> output_move = unit_load.production_moves.new()
    >>> output_move.product = aux_product
    >>> output_move.quantity = Decimal('4')
    >>> output_move.from_location = production_loc
    >>> output_move.to_location = new_stor
    >>> output_move.unit_price = aux_product.cost_price
    >>> unit_load.save()
    >>> len(unit_load.production_moves)
    3
    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> unit_load.available_cases_quantity
    5.0
    >>> UnitLoad.find([('available_cases_quantity', '=', 5)]) == [unit_load]
    True

Drop wizard::

    >>> drop_ul = Wizard('stock.unit_load.do_drop', [unit_load])
    >>> drop_ul.form.start_date = datetime.datetime.now() - relativedelta(days=2)
    >>> drop_ul.form.end_date != None
    True
    >>> drop_ul.form.location = storage_loc
    >>> drop_ul.form.warehouse_production == production_loc
    True
    >>> drop_ul.form.start_date = datetime.datetime.now() - relativedelta(days=20)
    >>> drop_ul.form.warehouse_production == production_loc
    True

State error::

    >>> drop_ul.execute('try_') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> unit_load.click('do')
    >>> unit_load.state
    'done'

Location error::

    >>> drop_ul.execute('try_') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> drop_ul.form.location = production_loc

Date error::

    >>> drop_ul.execute('try_') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> drop_ul.form.start_date = drop_ul.form.end_date - relativedelta(minutes=30)

Return location not defined::

    >>> drop_ul.execute('try_')
    >>> len(drop_ul.form.products)
    1
    >>> not drop_ul.form.products[0].location
    True
    >>> drop_ul.form.location = storage3
    >>> drop_ul.form.products[0].location != None
    True
    >>> drop_ul.execute('force')

Check moves::

    >>> unit_load.reload()
    >>> unit_load.state
    'draft'
    >>> len(unit_load.moves)
    6
    >>> len(unit_load.drop_moves)
    2
    >>> len(unit_load.return_moves)
    1
    >>> unit_load.return_moves[0].to_location.id == storage3.id
    True
    >>> bool(unit_load.dropped)
    False
    >>> unit_load.available_cases_quantity
    0.0

Cancel dropping::

    >>> unit_load.click('cancel')
    >>> unit_load.reload()
    >>> unit_load.state
    'done'
    >>> unit_load.available_cases_quantity
    5.0

Use default return location::

    >>> unit_load.warehouse = warehouse_loc
    >>> unit_load.save()
    >>> drop_ul = Wizard('stock.unit_load.do_drop', [unit_load])
    >>> drop_ul.form.location = production_loc
    >>> drop_ul.form.start_date = drop_ul.form.end_date - relativedelta(minutes=30)
    >>> drop_ul.execute('try_')
    >>> unit_load.reload()
    >>> unit_load.state
    'draft'
    >>> len(unit_load.moves)
    6
    >>> len(unit_load.drop_moves)
    2
    >>> len(unit_load.return_moves)
    1
    >>> unit_load.return_moves[0].to_location.id == storage_loc.id
    True

Confirm drop from wizard::

    >>> drop_ul = Wizard('stock.unit_load.do_drop', [unit_load])
    >>> drop_ul.form.drop_cases_quantity
    5.0
    >>> drop_ul.form.drop_cases_quantity = 2.0
    >>> drop_ul.execute('do_')
    >>> unit_load.reload()
    >>> len(unit_load.return_moves)
    0
    >>> len(unit_load.moves)
    5
    >>> Move = Model.get('stock.move')
    >>> return_moves = Move.find([('origin', '=', 'stock.unit_load,%s' % unit_load.id)])
    >>> not return_moves[0].unit_load
    True
    >>> not unit_load.return_moves
    True
    >>> bool(unit_load.dropped)
    False
    >>> bool(unit_load.available)
    True
    >>> bool(unit_load.drop_moves)
    True
    >>> drop_ul = Wizard('stock.unit_load.do_drop', [unit_load])
    >>> drop_ul.form.location = production_loc
    >>> drop_ul.form.start_date = drop_ul.form.end_date - relativedelta(minutes=12)
    >>> drop_ul.form.available_cases_quantity
    3.0
    >>> drop_ul.form.drop_cases_quantity
    3.0
    >>> drop_ul.execute('try_')
    >>> unit_load.reload()
    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> unit_load.reload()
    >>> bool(unit_load.dropped)
    True
    >>> bool(unit_load.available)
    False

Create another unit load::

    >>> unit_load = UnitLoad()
    >>> unit_load.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=15)
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.save()

Add moves::

    >>> unit_load.quantity = Decimal('35.0')
    >>> move = unit_load.production_moves[0]
    >>> move.to_location = new_stor
    >>> input_move = unit_load.production_moves.new()
    >>> input_move.product = aux_product
    >>> input_move.quantity = Decimal('4')
    >>> input_move.from_location = storage_loc
    >>> input_move.to_location = production_loc
    >>> output_move = unit_load.production_moves.new()
    >>> output_move.product = aux_product
    >>> output_move.quantity = Decimal('4')
    >>> output_move.from_location = production_loc
    >>> output_move.to_location = new_stor
    >>> output_move.unit_price = aux_product.cost_price
    >>> unit_load.warehouse = warehouse_loc
    >>> unit_load.save()
    >>> unit_load.click('assign')
    >>> unit_load.click('do')

Add ul location to user::

    >>> User = Model.get('res.user')
    >>> admin, = User.find([('login', '=', 'admin')])
    >>> admin.ul_drop_location = production_loc
    >>> admin.save()

Execute global drop wizard::

    >>> drop_ul = Wizard('stock.unit_load.do_drop', [])
    >>> drop_ul.form.ul_code = '123'
    >>> drop_ul.execute('pre_data') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: Cannot find Unit load "123".
    >>> drop_ul.form.ul_code = unit_load.code
    >>> drop_ul.execute('pre_data')

Check default data and behavior on global drop wizard::

    >>> drop_ul.form.location == production_loc
    True
    >>> drop_ul.form.end_date == None
    True
    >>> drop_ul.execute('try_')
    >>> unit_load.reload()
    >>> move = unit_load.drop_moves[0]
    >>> move.state
    'draft'
    >>> move.end_date == move.start_date
    True
    >>> time.sleep(1)
    >>> drop_ul.execute('do_')
    >>> move.reload()
    >>> move.state
    'done'
    >>> move.end_date > move.start_date
    True

Create another unit load::

    >>> unit_load = UnitLoad()
    >>> unit_load.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=15)
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.save()

Add moves::

    >>> unit_load.quantity = Decimal('35.0')
    >>> move = unit_load.production_moves[0]
    >>> move.to_location = new_stor
    >>> input_move = unit_load.production_moves.new()
    >>> input_move.product = aux_product
    >>> input_move.quantity = Decimal('4')
    >>> input_move.from_location = storage_loc
    >>> input_move.to_location = production_loc
    >>> output_move = unit_load.production_moves.new()
    >>> output_move.product = aux_product
    >>> output_move.quantity = Decimal('4')
    >>> output_move.from_location = production_loc
    >>> output_move.to_location = new_stor
    >>> output_move.unit_price = aux_product.cost_price
    >>> unit_load.click('assign')
    >>> unit_load.click('do')

Execute global drop wizard with force::

    >>> drop_ul = Wizard('stock.unit_load.do_drop', [])
    >>> drop_ul.form.ul_code = unit_load.code
    >>> drop_ul.execute('pre_data')
    >>> drop_ul.form.location = production_loc
    >>> drop_ul.form.start_date = datetime.datetime.now() - relativedelta(minutes=30)
    >>> drop_ul.execute('try_')
    >>> drop_ul.form.location = storage3
    >>> drop_ul.execute('force')
    >>> drop_ul.execute('do_')

Active stock configuration do ul drop::

    >>> Configuration = Model.get('stock.configuration')
    >>> configuration = Configuration(1)

    >>> bool(configuration.do_ul_drop)
    False
    >>> configuration.do_ul_drop = True
    >>> configuration.save()

Create new unit load and moves::

    >>> unit_load2 = UnitLoad()
    >>> unit_load2.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load2.end_date = unit_load.start_date + relativedelta(minutes=15)
    >>> unit_load2.production_type = 'location'
    >>> unit_load2.production_location = production_loc
    >>> unit_load2.product = product
    >>> unit_load2.cases_quantity = 5
    >>> unit_load2.save()

    >>> unit_load2.quantity = Decimal('35.0')
    >>> move2 = unit_load2.production_moves[0]
    >>> move2.to_location = new_stor
    >>> input_move2 = unit_load2.production_moves.new()
    >>> input_move2.product = aux_product
    >>> input_move2.quantity = Decimal('4')
    >>> input_move2.from_location = storage_loc
    >>> input_move2.to_location = production_loc
    >>> output_move2 = unit_load2.production_moves.new()
    >>> output_move2.product = aux_product
    >>> output_move2.quantity = Decimal('4')
    >>> output_move2.from_location = production_loc
    >>> output_move2.to_location = new_stor
    >>> output_move2.unit_price = aux_product.cost_price
    >>> unit_load2.warehouse = warehouse_loc
    >>> unit_load2.save()
    >>> unit_load2.click('assign')
    >>> unit_load2.click('do')
    >>> len(unit_load2.drop_moves)
    0

Drop with do ul drop active::

    >>> drop_ul = Wizard('stock.unit_load.do_drop', [unit_load2])
    >>> drop_ul.form.drop_cases_quantity = 2.0
    >>> drop_ul.form.end_date = None
    >>> drop_ul.execute('try_')
    >>> drop_ul.execute('end')
    >>> len(unit_load2.drop_moves)
    2
    >>> unit_load2.drop_moves[0].state
    'draft'

    >>> unit_load2.click('cancel')
    >>> len(unit_load2.drop_moves)
    0

    >>> drop_ul = Wizard('stock.unit_load.do_drop', [unit_load2])
    >>> drop_ul.form.drop_cases_quantity = 2.0
    >>> drop_ul.execute('try_')
    >>> len(unit_load2.drop_moves)
    2
    >>> unit_load2.drop_moves[0].state
    'done'


Active stock configuration propose drop end date with "start_date"::

    >>> ProductionConfiguration = Model.get('production.configuration')
    >>> prod_conf = ProductionConfiguration(1)
    >>> bool(prod_conf.propose_drop_end_date)
    False
    >>> prod_conf.propose_drop_end_date = 'start_date'
    >>> prod_conf.save()


Create new unit load and moves::

    >>> unit_load3 = UnitLoad()
    >>> unit_load3.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load3.end_date = unit_load.start_date + relativedelta(minutes=15)
    >>> unit_load3.production_type = 'location'
    >>> unit_load3.production_location = production_loc
    >>> unit_load3.product = product
    >>> unit_load3.cases_quantity = 5
    >>> unit_load3.save()
    >>> unit_load3.quantity = Decimal('35.0')
    >>> move = unit_load3.production_moves[0]
    >>> move.to_location = new_stor
    >>> input_move = unit_load3.production_moves.new()
    >>> input_move.product = aux_product
    >>> input_move.quantity = Decimal('4')
    >>> input_move.from_location = storage_loc
    >>> input_move.to_location = production_loc
    >>> output_move = unit_load3.production_moves.new()
    >>> output_move.product = aux_product
    >>> output_move.quantity = Decimal('4')
    >>> output_move.from_location = production_loc
    >>> output_move.to_location = new_stor
    >>> output_move.unit_price = aux_product.cost_price
    >>> unit_load3.warehouse = warehouse_loc
    >>> unit_load3.save()
    >>> unit_load3.click('assign')
    >>> unit_load3.click('do')


Execute global drop wizard with proposed end date::

    >>> drop_ul = Wizard('stock.unit_load.do_drop', [])
    >>> drop_ul.form.ul_code = unit_load3.code
    >>> drop_ul.execute('pre_data')
    >>> drop_ul.form.start_date != None
    True
    >>> drop_ul.form.end_date == drop_ul.form.start_date
    True
    >>> drop_ul.execute('try_')
    >>> unit_load3.reload()
    >>> move = unit_load.drop_moves[0]
    >>> move.state
    'done'


Active stock configuration propose drop end date with "last_drop"::

    >>> prod_conf.propose_drop_end_date = 'last_drop'
    >>> prod_conf.save()


Create another unit loads::

    >>> unit_load4 = UnitLoad()
    >>> unit_load4.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load4.end_date = unit_load4.start_date + relativedelta(minutes=15)
    >>> unit_load4.production_type = 'location'
    >>> unit_load4.production_location = production_loc
    >>> unit_load4.product = product
    >>> unit_load4.cases_quantity = 5
    >>> unit_load4.quantity = 35.0
    >>> move = unit_load4.production_moves[0]
    >>> move.to_location = new_stor
    >>> unit_load4.click('assign')
    >>> unit_load4.click('do')

    >>> unit_load5 = UnitLoad()
    >>> unit_load5.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load5.end_date = unit_load5.start_date + relativedelta(minutes=15)
    >>> unit_load5.production_type = 'location'
    >>> unit_load5.production_location = production_loc
    >>> unit_load5.product = product
    >>> unit_load5.cases_quantity = 5
    >>> unit_load5.quantity = 35.0
    >>> move = unit_load5.production_moves[0]
    >>> move.to_location = new_stor
    >>> unit_load5.click('assign')
    >>> unit_load5.click('do')

    >>> unit_load6 = UnitLoad()
    >>> unit_load6.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load6.end_date = unit_load6.start_date + relativedelta(minutes=15)
    >>> unit_load6.production_type = 'location'
    >>> unit_load6.production_location = production_loc
    >>> unit_load6.product = product
    >>> unit_load6.cases_quantity = 5
    >>> unit_load6.quantity = 35.0
    >>> move = unit_load6.production_moves[0]
    >>> move.to_location = new_stor
    >>> unit_load6.click('assign')
    >>> unit_load6.click('do')

    >>> unit_load7 = UnitLoad()
    >>> unit_load7.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load7.end_date = unit_load7.start_date + relativedelta(minutes=15)
    >>> unit_load7.production_type = 'location'
    >>> unit_load7.production_location = production_loc
    >>> unit_load7.product = product
    >>> unit_load7.cases_quantity = 5
    >>> unit_load7.quantity = 35.0
    >>> move = unit_load7.production_moves[0]
    >>> move.to_location = new_stor
    >>> unit_load7.click('assign')
    >>> unit_load7.click('do')

    >>> unit_load8 = UnitLoad()
    >>> unit_load8.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load8.end_date = unit_load8.start_date + relativedelta(minutes=15)
    >>> unit_load8.production_type = 'location'
    >>> unit_load8.production_location = production_loc
    >>> unit_load8.product = product
    >>> unit_load8.cases_quantity = 5
    >>> unit_load8.quantity = 35.0
    >>> move = unit_load8.production_moves[0]
    >>> move.to_location = new_stor
    >>> unit_load8.click('assign')
    >>> unit_load8.click('do')


Execute global drop wizard with proposed end date::

    >>> drop_ul = Wizard('stock.unit_load.do_drop', [])
    >>> drop_ul.form.ul_code = unit_load4.code
    >>> drop_ul.execute('pre_data')
    >>> drop_ul.form.location = production_loc2
    >>> not bool(drop_ul.form.end_date)
    True
    >>> drop_ul.execute('try_')
    >>> unit_load4.reload()
    >>> move, = unit_load4.drop_moves
    >>> not bool(move.end_date)
    True

    >>> time.sleep(5)
    >>> drop_ul = Wizard('stock.unit_load.do_drop', [])
    >>> drop_ul.form.ul_code = unit_load5.code
    >>> drop_ul.execute('pre_data')
    >>> drop_ul.form.location = production_loc2
    >>> drop_ul.form.parallel = True
    >>> drop_ul.execute('try_')
    >>> unit_load5.reload()
    >>> move2, = unit_load5.drop_moves
    >>> not bool(move2.end_date)
    True
    >>> move2.end_date = datetime.datetime.now()
    >>> move2.save()
    >>> move.reload()
    >>> move.state
    'draft'

    >>> time.sleep(5)
    >>> drop_ul = Wizard('stock.unit_load.do_drop', [])
    >>> drop_ul.form.ul_code = unit_load6.code
    >>> drop_ul.execute('pre_data')
    >>> drop_ul.form.location = production_loc2
    >>> drop_ul.form.parallel = True
    >>> drop_ul.execute('try_')
    >>> unit_load6.reload()
    >>> move3, = unit_load6.drop_moves
    >>> not bool(move3.end_date)
    True
    >>> move.reload()
    >>> move.state
    'draft'
    >>> move3.reload()
    >>> move3.state
    'draft'

    >>> time.sleep(5)
    >>> drop_ul = Wizard('stock.unit_load.do_drop', [])
    >>> drop_ul.form.ul_code = unit_load7.code
    >>> drop_ul.execute('pre_data')
    >>> drop_ul.form.location = production_loc2
    >>> drop_ul.form.parallel = True
    >>> drop_ul.execute('try_')
    >>> unit_load7.reload()
    >>> move4, = unit_load7.drop_moves
    >>> not bool(move4.end_date)
    True
    >>> move4.state
    'draft'
    >>> unit_load7.click('assign')
    >>> unit_load7.click('do') # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: Cannot do UL "..." with a move without end date. - 

    >>> time.sleep(5)
    >>> drop_ul = Wizard('stock.unit_load.do_drop', [])
    >>> drop_ul.form.ul_code = unit_load8.code
    >>> drop_ul.execute('pre_data')
    >>> drop_ul.form.location = production_loc2
    >>> now = datetime.datetime.now().replace(microsecond=0)
    >>> drop_ul.form.end_date = now
    >>> drop_ul.execute('try_')
    >>> move.reload()
    >>> move3.reload()
    >>> move4.reload()
    >>> bool(move.end_date)
    True
    >>> bool(move3.end_date)
    True
    >>> move3.end_date == now
    True
    >>> move3.state
    'done'
    >>> bool(move4.end_date)
    True
    >>> move4.end_date == now
    True
    >>> move4.state
    'done'

Check wizard error unit load not available::

    >>> bool(unit_load.available)
    False
    >>> drop_ul = Wizard('stock.unit_load.do_drop', [])
    >>> drop_ul.form.ul_code = unit_load.code
    >>> drop_ul.execute('pre_data') # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: Unit load "..." is not available. - 