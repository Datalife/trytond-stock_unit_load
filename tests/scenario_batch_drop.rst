======================
Batch Drop unit load
======================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard, Report
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> tomorrow = today + relativedelta(days=1)
    >>> time_ = datetime.datetime.now().time()
    >>> time_ = time_.replace(microsecond=0)

Install unit load Module::

    >>> config = activate_modules('stock_unit_load')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Check ul_drop_location in user::

    >>> admin = User(config.user)
    >>> admin.ul_drop_location == None
    True

Create main product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('8')
    >>> product.save()

Create another product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> template = ProductTemplate()
    >>> template.name = 'Aux. product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('0')
    >>> template.save()
    >>> aux_product, = template.products
    >>> aux_product.cost_price = Decimal('2')
    >>> aux_product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> new_stor = Location(name='Storage 2')
    >>> new_stor.parent = storage_loc
    >>> new_stor.save()
    >>> new_prod = Location(name='New production', type='production')
    >>> new_prod.save()
    >>> production_loc2 = Location(name='Production 2', type='production')
    >>> production_loc2.save()
    >>> wh2, = warehouse_loc.duplicate()
    >>> wh2.name = 'Warehouse 2'
    >>> wh2.code = 'WH2'
    >>> wh2.production_location = production_loc2
    >>> wh2.save()
    >>> new_stor2 = wh2.storage_location

Create an unit load::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=15)
    >>> unit_load.warehouse = warehouse_loc
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.save()

Add moves::

    >>> unit_load.quantity = Decimal('35.0')
    >>> move = unit_load.production_moves[0]
    >>> move.to_location = new_stor
    >>> input_move = unit_load.production_moves.new()
    >>> input_move.product = aux_product
    >>> input_move.quantity = Decimal('4')
    >>> input_move.from_location = storage_loc
    >>> input_move.to_location = production_loc
    >>> output_move = unit_load.production_moves.new()
    >>> output_move.product = aux_product
    >>> output_move.quantity = Decimal('4')
    >>> output_move.from_location = production_loc
    >>> output_move.to_location = new_stor
    >>> output_move.unit_price = aux_product.cost_price
    >>> unit_load.save()
    >>> len(unit_load.production_moves)
    3
    >>> unit_load.click('assign')
    >>> unit_load.click('do')

Create another unit load::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load2 = UnitLoad()
    >>> unit_load2.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load2.end_date = unit_load.start_date + relativedelta(minutes=15)
    >>> unit_load2.warehouse = warehouse_loc
    >>> unit_load2.production_type = 'location'
    >>> unit_load2.production_location = production_loc
    >>> unit_load2.product = product
    >>> unit_load2.cases_quantity = 5
    >>> unit_load2.save()

Add moves::

    >>> unit_load2.quantity = Decimal('35.0')
    >>> move = unit_load2.production_moves[0]
    >>> move.to_location = new_stor
    >>> input_move = unit_load2.production_moves.new()
    >>> input_move.product = aux_product
    >>> input_move.quantity = Decimal('4')
    >>> input_move.from_location = storage_loc
    >>> input_move.to_location = production_loc
    >>> output_move = unit_load2.production_moves.new()
    >>> output_move.product = aux_product
    >>> output_move.quantity = Decimal('4')
    >>> output_move.from_location = production_loc
    >>> output_move.to_location = new_stor
    >>> output_move.unit_price = aux_product.cost_price
    >>> unit_load2.save()
    >>> len(unit_load2.production_moves)
    3
    >>> unit_load2.click('assign')
    >>> unit_load2.click('do')

Batch drop wizard::

    >>> drop_ul = Wizard('stock.unit_load.batch_drop', [unit_load, unit_load2])
    >>> drop_ul.form.location == None
    True
    >>> drop_ul.form.location = new_prod
    >>> len(drop_ul.form.warehouse_productions)
    1
    >>> drop_ul.form.start_date = datetime.datetime.now() - relativedelta(hours=2)
    >>> drop_ul.form.delay_ = datetime.timedelta(minutes=25)
    >>> drop_ul.form.start_date = datetime.datetime.now() - relativedelta(hours=20)
    >>> drop_ul.form.end_date != None
    True
    >>> len(drop_ul.form.unit_loads)
    2
    >>> drop_ul.execute('confirm')
    >>> len(drop_ul.form.unit_loads)
    2
    >>> drop_ul.execute('do_')

Create unit loads with different location::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load3 = UnitLoad()
    >>> unit_load3.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load3.end_date = unit_load.start_date + relativedelta(minutes=15)
    >>> unit_load3.warehouse = warehouse_loc
    >>> unit_load3.production_type = 'location'
    >>> unit_load3.production_location = production_loc
    >>> unit_load3.product = product
    >>> unit_load3.cases_quantity = 5
    >>> unit_load3.save()
    >>> unit_load4 = UnitLoad()
    >>> unit_load4.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load4.end_date = unit_load.start_date + relativedelta(minutes=15)
    >>> unit_load4.warehouse = wh2
    >>> unit_load4.production_type = 'location'
    >>> unit_load4.production_location = production_loc2
    >>> unit_load4.product = product
    >>> unit_load4.cases_quantity = 5
    >>> unit_load4.save()

Add moves::

    >>> unit_load3.quantity = Decimal('35.0')
    >>> move = unit_load3.production_moves[0]
    >>> move.to_location = new_stor
    >>> input_move = unit_load3.production_moves.new()
    >>> input_move.product = aux_product
    >>> input_move.quantity = Decimal('4')
    >>> input_move.from_location = storage_loc
    >>> input_move.to_location = production_loc
    >>> input_move.unit_price = aux_product.cost_price
    >>> output_move = unit_load3.production_moves.new()
    >>> output_move.product = aux_product
    >>> output_move.quantity = Decimal('4')
    >>> output_move.from_location = production_loc
    >>> output_move.to_location = new_stor
    >>> output_move.unit_price = aux_product.cost_price
    >>> unit_load3.save()
    >>> len(unit_load3.production_moves)
    3
    >>> unit_load3.click('assign')
    >>> unit_load3.click('do')
    >>> unit_load4.quantity = Decimal('35.0')
    >>> move = unit_load4.production_moves[0]
    >>> move.to_location = new_stor2
    >>> input_move = unit_load4.production_moves.new()
    >>> input_move.product = aux_product
    >>> input_move.quantity = Decimal('4')
    >>> input_move.from_location = new_stor2
    >>> input_move.to_location = production_loc2
    >>> input_move.unit_price = aux_product.cost_price
    >>> output_move = unit_load4.production_moves.new()
    >>> output_move.product = aux_product
    >>> output_move.quantity = Decimal('4')
    >>> output_move.from_location = production_loc2
    >>> output_move.to_location = new_stor2
    >>> output_move.unit_price = aux_product.cost_price
    >>> unit_load4.save()
    >>> len(unit_load4.production_moves)
    3
    >>> unit_load4.click('assign')
    >>> unit_load4.click('do')

Assign ul_drop_location to admin::

    >>> admin.ul_drop_location = production_loc2
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)

Check different warehouse error::

    >>> drop_ul2 = Wizard('stock.unit_load.batch_drop', [unit_load3, unit_load4])
    >>> drop_ul2.form.location != None
    True
    >>> drop_ul2.form.location == production_loc2
    True
    >>> drop_ul2.form.start_date = datetime.datetime.now() - relativedelta(hours=2)
    >>> drop_ul2.form.delay_ = datetime.timedelta(minutes=25)
    >>> drop_ul2.form.end_date != None
    True
    >>> drop_ul2.execute('confirm')
    >>> drop_ul2.execute('do_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot move or drop UL "3" to location "Production 2" because it is on Warehouse "Warehouse". - 

Check moves::

    >>> Move = Model.get('stock.move')
    >>> unit_load.reload()
    >>> unit_load.state
    'done'
    >>> bool(unit_load.dropped)
    True
    >>> bool(unit_load.available)
    False
    >>> unit_load.location == new_prod
    True
    >>> len(unit_load.moves)
    5
    >>> len(unit_load.drop_moves)
    2
    >>> len(unit_load.return_moves)
    0
    >>> return_move, = Move.find([('origin', '=', 'stock.unit_load,%s' % unit_load.id)])
    >>> return_move not in unit_load.moves
    True
    >>> return_move.to_location.id == storage_loc.id
    True
    >>> unit_load2.reload()
    >>> bool(unit_load2.available)
    False
    >>> ((unit_load2.drop_moves[0].start_date - unit_load.drop_moves[0].start_date).seconds//60)%60
    25

Try to drop again::

    >>> drop_ul = Wizard('stock.unit_load.batch_drop', [unit_load])
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Unit load "1" is not available. - 