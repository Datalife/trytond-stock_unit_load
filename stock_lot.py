# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    def _get_inventory_move(self, move):
        inventory_move = super()._get_inventory_move(move)
        if inventory_move and move.unit_load:
            inventory_move.lot = move.lot
        return inventory_move


class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    def _get_new_move(self, move, default_values):
        new_move = super()._get_new_move(move, default_values)
        new_move.lot = move.lot
        return new_move

    def get_last_moves(self, name=None, product_id=None, location_type=None,
            at_date=None, check_start_date=False, move_states=[],
            return_ids=True, **kwargs):
        pool = Pool()
        Move = pool.get('stock.move')
        last_moves = super().get_last_moves(
            name=name, product_id=product_id, location_type=location_type,
            at_date=at_date, check_start_date=check_start_date,
            move_states=move_states, return_ids=return_ids, **kwargs)

        products_lots = kwargs.get('products_lots', None)
        if products_lots:
            for move in list(last_moves):
                if return_ids:
                    move = Move(move)
                product_lots = products_lots.get(move.product, None)
                if product_lots and not product_lots.get(move.lot, 0):
                    last_moves.remove(move.id if return_ids else move)
        return last_moves

    def _get_quantity_to_move(self, _grouped_moves, product, uom,
            cases_quantity, **kwargs):
        qty = super()._get_quantity_to_move(_grouped_moves, product, uom,
            cases_quantity, **kwargs)

        products_lots = kwargs.get('products_lots', {})
        if qty and products_lots.get(product, None):
            lots = products_lots[product].copy()
            qty = 0.0
            for move in _grouped_moves:
                qty += lots.get(move.lot, 0.0)
                lots[move.lot] = 0.0
        return qty
